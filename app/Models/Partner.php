<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'code',
        'name',
        'endpoint_url',
        'public_key',
        'secret_key',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
}
