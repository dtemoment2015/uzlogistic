<?php

namespace App\Models;

use App\Enums\CainiaoMethodEnum;
use App\Models\Order\Buyer;
use App\Models\Order\Parcel;
use App\Models\Order\Receiver;
use App\Models\Order\Sender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    use HasFactory, SoftDeletes;

    protected $fillable = [
        'type' => CainiaoMethodEnum::class,
        'logisticsOrderCode',
        'trackingNumber',
        'packageCode',
        'partner_id',
        'sender_id',
        'buyer_id',
        'receiver_id',
        'parcel_id',

    ];

    public function receiver()
    {
        return $this->belongsTo(Receiver::class);
    }

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function sender()
    {
        return $this->belongsTo(Sender::class);
    }
    
    public function parcel()
    {
        return $this->belongsTo(Parcel::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
}
