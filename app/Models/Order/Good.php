<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    protected $fillable = [
        'parcel_id',
        'name',
        'cnName',
        'skuID',
        'localName',
        'categoryName',
        'url',
        'price',
        'tax',
        'weight',
        'itemPrice',
        'quantity',
        'priceUnit',
        'weightUnit',
        'priceCurrency',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function parcel()
    {
        return $this->belongsTo(Parcel::class);
    }
}
