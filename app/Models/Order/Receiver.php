<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    use HasFactory;

    protected $fillable = [
        'imID',
        'name',
        'phone',
        'mobile',
        'email',
        'zipCode',
        'addressCountry',
        'addressProvince',
        'addressCity',
        'addressDistrict',
        'addressStreet',
        'cpreceiverCode',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
