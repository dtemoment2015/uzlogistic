<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bag extends Model
{
    use HasFactory;

    protected $fillable = [
        'bigBagID',
        'bigBagLength',
        'bigBagWidth',
        'bigBagHeigh',
        'bigBagDimensionUnit',
        'parcelQuantity',
        'bigBagWeight',
        'bigBagWeightUnit',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }
}
