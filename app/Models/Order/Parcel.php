<?php

namespace App\Models\Order;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parcel extends Model
{
    use HasFactory;

    protected $fillable = [
        'bag_id',
        'weight',
        'suggestedWeight',
        'payWeight',
        'itemWeight',
        'price',
        'priceUnit',
        'length',
        'width',
        'height',
        'weightUnit',
    ];

    public function goods()
    {
        return $this->hasMany(Good::class);
    }

    public function bag()
    {
        return $this->belongsTo(Bag::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
