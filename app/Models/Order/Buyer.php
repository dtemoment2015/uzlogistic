<?php

namespace App\Models\Order;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    use HasFactory;

    protected $fillable = [
        'imID',
        'name',
        'phone',
        'mobile',
        'email',
        'zipCode',
        'addressCountry',
        'addressProvince',
        'addressCity',
        'addressDistrict',
        'addressStreet',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
