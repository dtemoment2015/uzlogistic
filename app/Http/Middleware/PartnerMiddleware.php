<?php

namespace App\Http\Middleware;

use App\Models\Partner;
use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PartnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $partner = Partner::whereCode(optional($request)->code)->limit(1)->firstOrFail();

        switch ($partner->code) {
            case 'cainiao':
                $logistics_interface = (string) optional($request)->logistics_interface;

                if (!@simplexml_load_string($logistics_interface)) {
                    return new JsonResponse([
                        'success' => false,
                        'errorCode' => "422",
                        'errorMsg' => "Logistic interface not valid",
                    ], 422);
                }
                try {
                    $request->request->add([
                        'data' => json_decode(
                            json_encode(
                                simplexml_load_string(
                                    $logistics_interface,
                                    "SimpleXMLElement",
                                    LIBXML_NOCDATA
                                )
                            ),
                            TRUE
                        ),
                        'method' => optional($request)->msg_type,
                    ]);
                } catch (Exception $e) {
                    return new JsonResponse([
                        'success' => false,
                        'errorCode' => "422",
                        'errorMsg' => "Logistic interface not valid",
                    ], 422);
                }

                break;
        }

        $request->request->add([
            'partner' => $partner
        ]);


        return $next($request);
    }
}
