<?php

namespace App\Http\Controllers;

use App\Models\Order\Bag;
use App\Models\Order\Buyer;
use App\Models\Order\Receiver;
use App\Models\Order\Sender;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebHookController extends Controller
{

    public function index(Request $request)
    {
        $method = $request->method;
        $transaction =  $this->$method($request);

        return response()->json(
            [
                'succress' => (bool) !$transaction,
                'errorCode' => "",
                'errorMsg' => $transaction,
            ]
        );
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_ASN($request)
    {
        try {
            $order = DB::transaction(function () use ($request) {
                $data = $request->data;
                $order = $request
                    ->partner
                    ->orders()
                    ->firstOrCreate([
                        'logisticsOrderCode' => $data['logisticsOrderCode'],
                        'type' =>  $request->msg_type
                    ], $data);

                $order->type = $request->msg_type;
                $order->buyer()->associate($this->buyer($data['buyer']));
                $order->sender()->associate($this->sender($data['sender']));
                $order->receiver()->associate($this->receiver($data['receiver']));
                $order->parcel()->associate($this->parcel($data['parcel']));
                $order->save();
            });
            if ($order) {
                return null;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_CONSOLIDATIONDETAIL_NOTIFY($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_BATCHRECEIVING_NOTIFY($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_BATCHARRIVE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_BIGBAGRECEIVING_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_HOAIRLINE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_MAWBHOAIRLINE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_DEPARTURE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LINEHAUL_ARRIVAL_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_TRANSIT_ASN($request)
    {
        try {
            $order = DB::transaction(function () use ($request) {
                $data = $request->data;
                $order = $request
                    ->partner
                    ->orders()
                    ->firstOrCreate([
                        'logisticsOrderCode' => $data['logisticsOrderCode'],
                        'type' =>  $request->msg_type
                    ], $data);


                $order->type = $request->msg_type;
                $order->buyer()->associate($this->buyer($data['buyer']));
                $order->sender()->associate($this->sender($data['sender']));
                $order->receiver()->associate($this->receiver($data['receiver']));
                $order->parcel()->associate($this->parcel($data['parcel']));
                $order->save();
            });
            if ($order) {
                return null;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    protected function  CAINIAO_GLOBAL_TRANSIT_ARRIVAL_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_TRANSIT_POSTCOLLECTION_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_TRANSIT_DEPARTURE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_ASN($request)
    {
        try {
            $order = DB::transaction(function () use ($request) {
                $data = $request->data;
                $order = $request
                    ->partner
                    ->orders()
                    ->firstOrCreate([
                        'logisticsOrderCode' => $data['logisticsOrderCode'],
                        'type' =>  $request->msg_type
                    ], $data);


                $order->type = $request->msg_type;
                $order->buyer()->associate($this->buyer($data['buyer']));
                $order->sender()->associate($this->sender($data['sender']));
                $order->receiver()->associate($this->receiver($data['receiver']));
                $order->parcel()->associate($this->parcel($data['parcel']));
                $order->save();
            });
            if ($order) {
                return null;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_GTMS_SC_ARRIVE_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_GTMSACCEPT_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_GTMSDELIVERYOFFICEARRIVAL_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_GTMS_REDELIVERING_CALLBACK($request)
    {
    }

    protected function  CAINIAO_GLOBAL_LASTMILE_GTMSSIGN_CALLBACK($request)
    {
    }

    private function buyer($data)
    {
        return Buyer::firstOrCreate(
            [
                'imID' => optional($data)['imID'],
                'name' => optional($data)['name'],
                'phone' => optional($data)['phone'],
                'mobile' => optional($data)['mobile'],
                'email' => optional($data)['email'],
                'zipCode' => optional($data)['zipCode'],
                'addressCountry' => optional(optional($data)['address'])['country'],
                'addressProvince' => optional(optional($data)['address'])['province'],
                'addressCity' => optional(optional($data)['address'])['city'],
                'addressStreet' => optional(optional($data)['address'])['street'],
            ]
        );
    }

    private function sender($data)
    {
        return Sender::firstOrCreate(
            [
                'imID' => optional($data)['imID'],
                'name' => optional($data)['name'],
                'phone' => optional($data)['phone'],
                'mobile' => optional($data)['mobile'],
                'email' => optional($data)['email'],
                'zipCode' => optional($data)['zipCode'],
                'companyName' => optional($data)['companyName'],
                'addressCountry' => optional(optional($data)['address'])['country'],
                'addressProvince' => optional(optional($data)['address'])['province'],
                'addressCity' => optional(optional($data)['address'])['city'],
                'addressStreet' => optional(optional($data)['address'])['street'],
            ]
        );
    }
    private function receiver($data)
    {
        return Receiver::firstOrCreate(
            [
                'imID' => optional($data)['imID'],
                'name' => optional($data)['name'],
                'phone' => optional($data)['phone'],
                'mobile' => optional($data)['mobile'],
                'email' => optional($data)['email'],
                'zipCode' => optional($data)['zipCode'],
                'cpreceiverCode' => optional($data)['cpreceiverCode'],
                'addressCountry' => optional(optional($data)['address'])['country'],
                'addressProvince' => optional(optional($data)['address'])['province'],
                'addressCity' => optional(optional($data)['address'])['city'],
                'addressStreet' => optional(optional($data)['address'])['street'],
            ]
        );
    }

    private function parcel($data)
    {

        $parcel = null;

        $bag = Bag::firstOrCreate(

            ['bigBagID' => optional($data)['bigBagID']],
            [
                'bigBagLength' => optional($data)['bigBagLength'],
                'bigBagWidth' => optional($data)['bigBagWidth'],
                'bigBagHeigh' => optional($data)['bigBagHeigh'],
                'bigBagDimensionUnit' => optional($data)['bigBagDimensionUnit'],
                'parcelQuantity' => optional($data)['parcelQuantity'],
                'bigBagWeight' => optional($data)['bigBagWeight'],
                'bigBagWeightUnit' => optional($data)['bigBagWeightUnit'],
            ]
        );

        $parcel = $bag->parcels()->firstOrCreate(
            [
                'weight' =>  (float)  optional($data)['weight'],
                'suggestedWeight' =>  (float)  optional($data)['suggestedWeight'],
                'payWeight' =>  (float)  optional($data)['payWeight'],
                'itemWeight' =>  (float)  optional($data)['itemWeight'],
                'price' =>  (float)  optional($data)['price'],
                'priceUnit' => optional($data)['priceUnit'],
                'length' => (float) optional($data)['length'],
                'width' => (float) optional($data)['width'],
                'height' => (float) optional($data)['height'],
                'weightUnit' => optional($data)['weightUnit'],
            ]
        );

        if ($parcel->wasRecentlyCreated) {
            $goods = [];
            foreach ((array) optional(optional($data)['goodsList'])['goods'] as  $good) {
                $goods[] = [
                    'name' =>  optional($good)['name'],
                    'cnName' => optional($good)['cnName'],
                    'skuID' =>  optional($good)['skuID'],
                    'localName' => optional($good)['localName'],
                    'categoryName' => optional($good)['categoryName'],
                    'url' => optional($good)['url'],
                    'price' => optional($good)['price'],
                    'tax' => (float) optional($good)['tax'],
                    'weight' => optional($good)['weight'],
                    'itemPrice' => optional($good)['itemPrice'],
                    'quantity' => optional($good)['quantity'],
                    'priceUnit' => optional($good)['priceUnit'],
                    'weightUnit' => optional($good)['weightUnit'],
                    'priceCurrency' => optional($good)['priceCurrency'],
                ];
            }
            $parcel->goods()->createMany(
                $goods
            );
        }


        return $parcel;
    }
}
