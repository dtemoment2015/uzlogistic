<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(
        LoginRequest $request,
        User $user
    ) {
        $user = $user->whereEmail($request->email)->first();
        if (
            !Hash::check($request->password, $user->password)
        ) {
            return response()->json([
                'message' =>  __(
                    'message.access_error'
                ),
            ], 422);
        }
        
        $token = $user->createToken('user');

        return (new UserResource($user))
            ->response()
            ->header(
                'X-Access-Token',
                optional($token)->accessToken
            )
            ->header(
                'X-Expires-At',
                optional(optional($token)->token)['expires_at']
            );
    }
}
