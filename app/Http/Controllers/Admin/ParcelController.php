<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ParcelResource;
use App\Models\Order\Parcel;
use Illuminate\Http\Request;

class ParcelController extends Controller
{
    public function index(Parcel $Parcel)
    {
        return ParcelResource::collection(
            $Parcel->with(['bag', 'orders'])->paginate()
        );
    }

    public function show(Parcel $Parcel)
    {
        return new ParcelResource(
            $Parcel->load(['bag', 'orders'])
        );
    }

    public function store(Request $request, Parcel $Parcel)
    {
        return new ParcelResource(
            $Parcel->create(
                $request->all()
            )
        );
    }

    public function update(Request $request, Parcel $Parcel)
    {
        return response()->json([
            'updated' =>  $Parcel->update($request->all())
        ]);
    }

    public function destroy(Parcel $Parcel)
    {
        return response()->json([
            'deleted' => $Parcel->delete()
        ]);
    }
}
