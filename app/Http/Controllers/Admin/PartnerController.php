<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddPartnerRequest;
use App\Http\Resources\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class PartnerController extends Controller
{

    public function index(Partner $partner)
    {
        return PartnerResource::collection(
            $partner->paginate()
        );
    }

    public function show(Partner $partner)
    {
        return new PartnerResource(
            $partner
        );
    }

    public function store(AddPartnerRequest $request, Partner $partner)
    {

        $item = $partner->create([
            ...$request->all(),
            'uuid' => Str::orderedUuid()
        ]);
        
        return new PartnerResource(
            $item
        );
    }

    public function update(Request $request, Partner $partner)
    {
        return response()->json([
            'updated' =>  $partner->update($request->all())
        ]);
    }

    public function destroy(Partner $partner)
    {
        return response()->json([
            'deleted' => $partner->delete()
        ]);
    }
}
