<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReceiverResource;
use App\Models\Order\Receiver;
use Illuminate\Http\Request;

class ReceiverController extends Controller
{
    public function index(Receiver $Receiver)
    {
        return ReceiverResource::collection(
            $Receiver->paginate()
        );
    }

    public function show(Receiver $Receiver)
    {
        return new ReceiverResource(
            $Receiver
        );
    }

    public function store(Request $request, Receiver $Receiver)
    {

        $item = $Receiver->create(
            $request->all()
        );

        return new ReceiverResource(
            $item
        );
    }

    public function update(Request $request, Receiver $Receiver)
    {
        return response()->json([
            'updated' =>  $Receiver->update($request->all())
        ]);
    }

    public function destroy(Receiver $Receiver)
    {
        return response()->json([
            'deleted' => $Receiver->delete()
        ]);
    }
}
