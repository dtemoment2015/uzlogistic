<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Order $Order)
    {
        return OrderResource::collection(
            $Order
            ->with([
                'partner',
                'parcel',
                'buyer',
                'receiver',
                'sender',
            ])
            ->paginate()
        );
    }
    
    public function show(Order $Order)
    {
        return new OrderResource(
            $Order->load([
                'partner',
                'parcel',
                'buyer',
                'receiver',
                'sender',
            ])
        );
    }

    public function store(Request $request, Order $Order)
    {
        return new OrderResource(
            $Order->create(
                $request->all()
            )
        );
    }

    public function update(Request $request, Order $Order)
    {
        return response()->json([
            'updated' =>  $Order->update($request->all())
        ]);
    }

    public function destroy(Order $Order)
    {
        return response()->json([
            'deleted' => $Order->delete()
        ]);
    }
}
