<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\BuyerResource;
use App\Models\Order\Buyer;
use Illuminate\Http\Request;

class BuyerController extends Controller
{
    public function index(Buyer $Buyer)
    {
        return BuyerResource::collection(
            $Buyer->paginate()
        );
    }

    public function show(Buyer $Buyer)
    {
        return new BuyerResource(
            $Buyer
        );
    }

    public function store(Request $request, Buyer $Buyer)
    {
        return new BuyerResource(
            $Buyer->create(
                $request->all()
            )
        );
    }

    public function update(Request $request, Buyer $Buyer)
    {
        return response()->json([
            'updated' =>  $Buyer->update($request->all())
        ]);
    }

    public function destroy(Buyer $Buyer)
    {
        return response()->json([
            'deleted' => $Buyer->delete()
        ]);
    }
}
