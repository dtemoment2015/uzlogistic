<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Order\Buyer;
use App\Models\Order\Good;
use App\Models\Order\Receiver;
use App\Models\Order\Sender;

class StatController extends Controller
{
    public function index()
    {
        return response()->json(
            ['data' => [
                [
                    'key' => 'orders',
                    'title' => 'Всего заказов',
                    'icon' => 'ti-shopping-cart-full',
                    'value' => Order::count()
                ],
                [
                    'key' => 'goods',
                    'title' => 'Всего проведено товаров',
                    'icon' => 'ti-id-badge',
                    'value' => Good::count()
                ],
                [
                    'key' => 'goods',
                    'title' => 'Всего отправителей (компании)',
                    'icon' => 'ti-id-badge',
                    'value' => Sender::count()
                ],
                [
                    'key' => 'buyers',
                    'title' => 'Всего покупателей (клиенты)',
                    'icon' => 'ti-id-badge',
                    'value' => Buyer::count()
                ],
                [
                    'key' => 'goods',
                    'title' => 'Всего получателей (компании) ',
                    'icon' => 'ti-id-badge',
                    'value' => Receiver::count()
                ],
            ],]
        );
    }
}
