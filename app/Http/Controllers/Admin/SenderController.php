<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SenderResource;
use App\Models\Order\Sender;
use Illuminate\Http\Request;

class SenderController extends Controller
{
    public function index(Sender $Sender)
    {
        return SenderResource::collection(
            $Sender->paginate()
        );
    }

    public function show(Sender $Sender)
    {
        return new SenderResource(
            $Sender
        );
    }

    public function store(Request $request, Sender $Sender)
    {

        $item = $Sender->create(
            $request->all()
        );

        return new SenderResource(
            $item
        );
    }

    public function update(Request $request, Sender $Sender)
    {
        return response()->json([
            'updated' =>  $Sender->update($request->all())
        ]);
    }

    public function destroy(Sender $Sender)
    {
        return response()->json([
            'deleted' => $Sender->delete()
        ]);
    }
}
