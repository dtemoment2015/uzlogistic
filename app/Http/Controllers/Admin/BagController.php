<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\BagResource;
use App\Models\Order\Bag;
use Illuminate\Http\Request;

class BagController extends Controller
{
    public function index(Bag $Bag)
    {
        return BagResource::collection(
            $Bag->paginate()
        );
    }

    public function show(Bag $Bag)
    {
        return new BagResource(
            $Bag
        );
    }

    public function store(Request $request, Bag $Bag)
    {
        return new BagResource(
            $Bag->create(
                $request->all()
            )
        );
    }

    public function update(Request $request, Bag $Bag)
    {
        return response()->json([
            'updated' =>  $Bag->update($request->all())
        ]);
    }

    public function destroy(Bag $Bag)
    {
        return response()->json([
            'deleted' => $Bag->delete()
        ]);
    }
}
