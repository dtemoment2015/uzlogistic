<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\GoodResource;
use App\Models\Order\Good;
use Illuminate\Http\Request;

class GoodController extends Controller
{
    public function index(Good $Good)
    {
        return GoodResource::collection(
            $Good->with('parcel')->paginate()
        );
    }

    public function show(Good $Good)
    {
        return new GoodResource(
            $Good->load('parcel')
        );
    }

    public function store(Request $request, Good $Good)
    {
        return new GoodResource(
            $Good->create(
                $request->all()
            )
        );
    }

    public function update(Request $request, Good $Good)
    {
        return response()->json([
            'updated' =>  $Good->update($request->all())
        ]);
    }

    public function destroy(Good $Good)
    {
        return response()->json([
            'deleted' => $Good->delete()
        ]);
    }
}
