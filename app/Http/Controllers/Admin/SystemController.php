<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    public function index(){
        return response()->json(
          [
              'data' =>  [
                'routes' => [
                    [
                        'icon' => '',
                        'title' => 'Главная',
                        'path' => 'dashboard',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Заказы',
                        'path' => 'orders',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Партнеры',
                        'path' => 'partners',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Посылки',
                        'path' => 'parcels',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Отправители',
                        'path' => 'senders',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Покупатели',
                        'path' => 'buyers',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Получатели',
                        'path' => 'receivers',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Вещи',
                        'path' => 'goods',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Мешки',
                        'path' => 'bags',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Пользователи',
                        'path' => 'users',
                    ],
                    [
                        'icon' => '',
                        'title' => 'Выход',
                        'path' => 'quit',
                    ],
                ]
          ] 
            ]
        );
    }
}
