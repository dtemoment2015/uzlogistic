<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(User $User)
    {
        return UserResource::collection(
            $User->paginate()
        );
    }

    public function show(User $User)
    {
        return new UserResource(
            $User
        );
    }

    public function store(Request $request, User $User)
    {

        $item = $User->create(
            $request->all()
        );

        return new UserResource(
            $item
        );
    }

    public function update(Request $request, User $User)
    {
        return response()->json([
            'updated' =>  $User->update($request->all())
        ]);
    }

    public function destroy(User $User)
    {
        return response()->json([
            'deleted' => $User->delete()
        ]);
    }
}
