<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bags', function (Blueprint $table) {
            $table->id();
            $table->string('bigBagID')->nullable();
            $table->string('bigBagLength')->nullable();
            $table->string('bigBagWidth')->nullable();
            $table->string('bigBagHeigh')->nullable();
            $table->string('bigBagDimensionUnit')->nullable();
            $table->string('parcelQuantity')->nullable();
            $table->string('bigBagWeight')->nullable();
            $table->string('bigBagWeightUnit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bags');
    }
};
