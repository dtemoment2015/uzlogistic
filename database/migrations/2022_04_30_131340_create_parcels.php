<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->id();
            $table->float('weight', 32)->nullable();
            $table->float('suggestedWeight', 32)->nullable();
            $table->float('payWeight', 32)->nullable();
            $table->float('itemWeight', 32)->nullable();
            $table->float('price', 32)->nullable();
            $table->string('priceUnit', 32)->nullable();
            $table->float('length', 32)->nullable();
            $table->float('width', 32)->nullable();
            $table->float('height', 32)->nullable();
            $table->string('weightUnit', 64)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
};
