<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('logisticsOrderCode')->nullalbe();
            $table->string('trackingNumber')->nullalbe();
            $table->string('packageCode')->nullalbe();
            $table->bigInteger('partner_id')->index()->unsigned();
            $table->bigInteger('sender_id')->nullable()->unsigned();
            $table->bigInteger('buyer_id')->nullable()->unsigned();
            $table->bigInteger('receiver_id')->nullable()->unsigned();
            $table->bigInteger('parcel_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
