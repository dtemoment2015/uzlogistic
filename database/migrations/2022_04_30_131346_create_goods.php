<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parcel_id')->index()->nullable()->unsigned();
            $table->string('name')->nullable();
            $table->string('cnName')->nullable();
            $table->string('skuID')->nullable();
            $table->string('localName')->nullable();
            $table->string('categoryName')->nullable();
            $table->string('url')->nullable();
            $table->float('price', 32)->nullable();
            $table->float('tax', 32)->nullable();
            $table->float('weight', 32)->nullable();
            $table->float('itemPrice', 32)->nullable();
            $table->float('quantity', 32)->nullable();
            $table->string('priceUnit')->nullable();
            $table->string('weightUnit')->nullable();
            $table->string('priceCurrency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
};
