<?php

use App\Http\Controllers\Admin\BagController;
use App\Http\Controllers\Admin\BuyerController;
use App\Http\Controllers\Admin\GoodController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ParcelController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ReceiverController;
use App\Http\Controllers\Admin\SenderController;
use App\Http\Controllers\Admin\StatController;
use App\Http\Controllers\Admin\SystemController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\WebHookController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {

    //ENDPOINT URL FOR PARTNERS
    Route::post('partners/{code}', [WebHookController::class, 'index'])->middleware('partner');

    //ADMIN CONTROL
    Route::prefix('admin')->group(function () {
        Route::post('auth/login', [AuthController::class, 'login']);
        Route::get('system', [SystemController::class, 'index']);
        Route::middleware('auth:user')->group(function () {

            Route::resources(
                [
                    'partners' => PartnerController::class,
                    'orders' => OrderController::class,
                    'parcels' => ParcelController::class,
                    'receivers' => ReceiverController::class,
                    'buyers' => BuyerController::class,
                    'buyers' => BuyerController::class,
                    'users' => UserController::class,
                    'senders' => SenderController::class,
                    'goods' => GoodController::class,
                    'bags' => BagController::class,
                ]
            );
            
            Route::get('profile', [ProfileController::class, 'index']);
            Route::get('stats', [StatController::class, 'index']);
        });
    });
});
